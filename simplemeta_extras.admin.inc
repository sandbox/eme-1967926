<?php 

/**
 * retrieving all existing node types
 * @return array $nodeTypes
 */
function get_all_node_types(){
	return array_keys(node_type_get_types());;
}

/**
 * Creating vocabularies list to enable supporting it by node type
 * @todo automatically find which term is used by every node type
 * @return string $form
 */
function form_simplemeta_extra_node_type_settings() {
	//calling simplemeta module avaibles functions
	module_load_include('inc', 'simplemeta',  'simplemeta.admin');
	//getting all avaibles languages
	$languagesList = _simplemeta_langauge_list();
	// asking simplemeta module if the multilanguage support is enabled
	$langEnabled = variable_get('simplemeta_language_enable', FALSE);
	//retrieving all node types
	$nodeTypes = get_all_node_types();

 foreach($nodeTypes as $nodeType){
 	$form['group_' . $nodeType] = 
 	array(
 			'#type' => 'fieldset',
 			'#title' => $nodeType,
 			//'#weight' => 5,
 			'#collapsible' => TRUE,
 			'#collapsed' => FALSE,
 			'#group' => 'simplemeta_extra_' . $nodeType,
 	);
 	$form['group_' . $nodeType]['add_meta_'.  $nodeType] = 
 	array(
 			'#type' => 'button',
 			'#value' => t('Add meta'),
 			//'#weight' => 19,
 			'#attributes' => array(
 					'id'=>  array('add-meta-' . $nodeType),
 					 'class' =>  array('simplemeta-add-meta'),
 					'onClick' =>  array('alert(this.id); return false;'),
 					),
 			'#group' => 'simplemeta_extra_' . $nodeType,
 	);
 	$form['#attached']['css'] = array(
 			drupal_get_path('module', 'simplemeta_extras') . '/css/simplemeta_extras.css',
 	);
 	$form['#attached']['js'] = array(
 			drupal_get_path('module', 'simplemeta_extras') . '/js/simplemeta_extras-add-meta.js',
 	);
	
 }
	//return system_settings_form($form);
 // Add vertical tabs display if available.
 $form['#pre_render'][] = 'vertical_tabs_form_pre_render';
 
 return $form;
}

function simplemeta_extra_settings_form() {
	$form['simplemeta_extra'] = array(
			'#type' => 'checkbox',
			'#title' => t('Setting '),
			'#default_value' => variable_get('simplemeta_extra_setting', 0),
			'#group' => 'simplemeta_extra',
	);
	
	return $form;

	
}
/**
 * page callback admin/content/simplemeta_extra
 * main administration page function
 */
function simplemeta_extra_meta_list(){
	//retrieving drupal global $language variable
	global $language;
	//current language
	$lang = $language->language;
	
	//form
	return drupal_get_form('form_simplemeta_extra_node_type_settings');
}

function simplemeta_extra_get_first_attributes(){
	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		echo json_encode(variable_get('simplemeta_extras_first_attribute_list', array()));
	}
}
//Dynamically discover all meta first attributes and create a function for retrieve their default values
$simpleMetaExtraFunctions = array();

foreach(variable_get('simplemeta_extras_first_attribute_list',array(0)) as $firstAttributeName){
	$attributeName = trim(strtolower($firstAttributeName));
	$attributeName = str_replace(' ', '_', $attributeName);
	$simpleMetaExtraFunctions[$attributeName] = create_function('', 'if (!empty($_SERVER[\'HTTP_X_REQUESTED_WITH\']) && strtolower($_SERVER[\'HTTP_X_REQUESTED_WITH\']) == \'xmlhttprequest\') {
		echo json_encode(variable_get(\'simplemeta_extras_values_for_'.$attributeName.'\', array()));
	}');
}



